# Game of life (AP Computer Science Principles 2024 Performance Task)

## Description
This is Terminal User Interface program written in C that simulates a popular cellular automata called Game of Life (or simply, Life). It was invented by John H Conway. It has simple rules however the patterns can get very complex. It is Turing Complete and can simulate [Universal Constructor](https://en.wikipedia.org/wiki/Von_Neumann_universal_constructor). Due to limitations of terminal environment, you obviously are not able to create such complex configurations, however ... todo
## TOC
<!-- TOC -->
* [Game of life (AP Computer Science Principles 2024 Performance Task)](#game-of-life-ap-computer-science-principles-2024-performance-task)
  * [Description](#description)
  * [TOC](#toc)
  * [Build](#build)
  * [Usage](#usage)
    * [Input file example #1 (Gosper's Glider Gun)](#input-file-example-1-gospers-glider-gun)
    * [Glider](#glider)
    * [Light Weight Space ship (LWSS)](#light-weight-space-ship-lwss)
  * [License](#license)
<!-- TOC -->
## Build
GNU Make is required. This project can be built with either Clang from LLVM toolchain or C compiler from GCC.

## Usage
### Input file example #1 (Gosper's Glider Gun)
This pattern generates gliders and is one of the most famous patterns in the Game of Life. Save the following grid in a text file (e.g., gospers_glider_gun.cells):
```
........................O...........
......................O.O...........
............OO......OO............OO
...........O...O....OO............OO
OO........O.....O...OO..............
OO........O...O.OO....O.O...........
..........O.....O.......O...........
...........O...O....................
............OO......................
```
***
### Glider
A simple glider moves diagonally across the grid. Save this in a file (e.g., glider.cells):
```
.O..
..O.
OOO.
....
```
***
### Light Weight Space ship (LWSS)
This is a pattern that moves horizontally to the right. Save it in a file (e.g., lwss.cells):
```
.O..O
O....
O...O
OOOO.
```

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.