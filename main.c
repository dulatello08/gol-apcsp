/**
 * Conway's Game of Life - Terminal Version
 *
 * Compilation:
 *   Compile with: gcc -o game_of_life main.c  gcc -o game_of_life main.c -DDEFAULT_PADDING=200
 *   This compiles the program with a default padding of 200. You can adjust the padding by changing the number.
 *
 * Running:
 *   Run with: ./game_of_life path/to/gol_conf.cells
 *   Ensure that the provided path points to a valid configuration file.
 *
 * Controls:
 *   q - Quit the program
 *   p - Pause/resume the simulation
 *   s - Restart the simulation with the initial configuration
 *   Arrow Up - Increase grid size
 *   Arrow Down - Decrease grid size
 *
 * This program simulates John Conway's Game of Life in a terminal window, using input from a file
 * for initial grid setup and keyboard input for real-time control of the simulation.
 */

#define X_PADDING (DEFAULT_PADDING + 10)           // 10 extra padding on each side for width
#define Y_PADDING (DEFAULT_PADDING * 3 / 4)        // 75% of base padding for height

#define START_X_OFFSET (DEFAULT_PADDING / 2)       // Half of base padding for starting x-coordinate
#define START_Y_OFFSET (DEFAULT_PADDING * 7 / 20)  // 35% of base padding for starting y-coordinate

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <limits.h>
#include <termios.h>

//Define global variable that holds original configuration of terminal emulator when program starts up
struct termios orig_termios;

typedef struct {
    int width;
    int height;
    int **cells;
} Grid;

// Disable raw mode for the terminal.
// This function resets the terminal settings to the original configuration stored in orig_termios.
void disable_raw_mode() {
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios);
}

// Enable raw mode for the terminal.
// This function changes terminal settings to disable echoing of characters and enable continuous input reading.
// Settings are modified to not wait for a newline before reading inputs.
void enable_raw_mode() {
    tcgetattr(STDIN_FILENO, &orig_termios);
    atexit(disable_raw_mode);

    struct termios raw = orig_termios;
    raw.c_lflag &= ~(ECHO | ICANON);
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

// Resize the grid to new dimensions.
// This function (procedure) reallocates the grid's cell array to match new specified dimensions, copying over existing cell data.
// If the new dimensions are smaller, data beyond the new limits is lost.
// Parameters:
// - Grid *grid: A pointer to the grid structure to modify.
// - int new_width: The new width of the grid.
// - int new_height: The new height of the grid.
void resize_grid(Grid *grid, int new_width, int new_height) {
    // Allocate new cells array
    int **new_cells = malloc(new_height * sizeof(int *));
    for (int i = 0; i < new_height; i++) {
        new_cells[i] = calloc(new_width, sizeof(int));
    }

    // Copy old data to new cells
    int min_width = new_width < grid->width ? new_width : grid->width;
    int min_height = new_height < grid->height ? new_height : grid->height;
    for (int y = 0; y < min_height; y++) {
        memcpy(new_cells[y], grid->cells[y], min_width * sizeof(int));
    }

    // Free old cells
    for (int i = 0; i < grid->height; i++) {
        free(grid->cells[i]);
    }
    free(grid->cells);

    // Assign new cells and sizes to grid
    grid->cells = new_cells;
    grid->width = new_width;
    grid->height = new_height;
}

// Display the grid in the terminal along with user instructions.
// This function prints each cell of the grid to the terminal, using '.' for dead cells and 'O' for live cells. This function buffers output to prevent flickering.
// Grid boundaries are also displayed along with instructions for user inputs.
// Parameters:
// - Grid *grid: A pointer to the grid structure to display.
void display_grid(Grid *grid) {
    // Buffer to hold the entire output, adjust the size as needed
    int bufferSize = (grid->width + 3) * (grid->height + 4) + 200;
    char *buffer = (char *)malloc(bufferSize * sizeof(char));
    if (!buffer) {
        perror("Failed to allocate buffer");
        return;
    }

    // Use a pointer to append to the buffer
    char *ptr = buffer;

    // ANSI escape code to clear screen and reset cursor position
    ptr += sprintf(ptr, "\033[H\033[J");

    // Top border
    ptr += sprintf(ptr, "+");
    for (int x = 0; x < grid->width; x++) {
        ptr += sprintf(ptr, "-");
    }
    ptr += sprintf(ptr, "+\n");

    // Grid cells
    for (int y = 0; y < grid->height; y++) {
        ptr += sprintf(ptr, "|");
        for (int x = 0; x < grid->width; x++) {
            ptr += sprintf(ptr, "%c", grid->cells[y][x] ? 'O' : '.');
        }
        ptr += sprintf(ptr, "|\n");
    }

    // Bottom border
    ptr += sprintf(ptr, "+");
    for (int x = 0; x < grid->width; x++) {
        ptr += sprintf(ptr, "-");
    }
    ptr += sprintf(ptr, "+\n");

    // Instructions
    ptr += sprintf(ptr, "\nControls:\n");
    ptr += sprintf(ptr, "  q - Quit the program\n");
    ptr += sprintf(ptr, "  p - Pause/resume the simulation\n");
    ptr += sprintf(ptr, "  s - Restart the simulation with the initial configuration\n");
    ptr += sprintf(ptr, "  Arrow Up - Increase grid size\n");
    ptr += sprintf(ptr, "  Arrow Down - Decrease grid size if larger than minimum size\n");
    // Ensure the buffer is null-terminated
    ptr += sprintf(ptr, "\0");

    // Print the entire buffer at once
    printf("%s", buffer);
    free(buffer);
}

// Create a new grid with specified dimensions.
// This function allocates memory for a new grid and its cells, initializing all cells to dead (.).
// Parameters:
// - int width: The width of the new grid.
// - int height: The height of the new grid.
// Returns: A pointer to the newly created Grid structure.
Grid *create_grid(int width, int height) {
    Grid *grid = malloc(sizeof(Grid));
    grid->width = width;
    grid->height = height;
    grid->cells = malloc(height * sizeof(int *));
    for (int i = 0; i < height; i++) {
        grid->cells[i] = calloc(width, sizeof(int));
    }
    return grid;
}

// Free memory allocated for a grid.
// This function deallocates all memory used for the grid's cells and the grid structure itself. This step is extremely important to avoid memory leaks.
// Parameters:
// - Grid *grid: A pointer to the grid structure to free.
void free_grid(Grid *grid) {
    for (int i = 0; i < grid->height; i++) {
        free(grid->cells[i]);
    }
    free(grid->cells);
    free(grid);
}

// Update grid according to the Game of Life rules.
// This function iterates over each cell, counts live neighbors, and updates the cell's state based on the rules:
// 1. Any live cell with fewer than two live neighbors dies (underpopulation).
// 2. Any live cell with two or three live neighbors lives on to the next generation.
// 3. Any live cell with more than three live neighbors dies (overpopulation).
// 4. Any dead cell with exactly three live neighbors becomes a live cell (reproduction).
// Parameters:
// - Grid *grid: A pointer to the grid structure containing current game state.
void update_grid(Grid *grid) {
    Grid *new_grid_struct = create_grid(grid->width, grid->height);
    int **new_grid = new_grid_struct->cells;
    for (int y = 0; y < grid->height; y++) {
        for (int x = 0; x < grid->width; x++) {
            int live_neighbors = 0;
            for (int dy = -1; dy <= 1; dy++) {
                for (int dx = -1; dx <= 1; dx++) {
                    if (dx == 0 && dy == 0) continue;
                    int nx = (x + dx + grid->width) % grid->width;
                    int ny = (y + dy + grid->height) % grid->height;
                    live_neighbors += grid->cells[ny][nx];
                }
            }

            if (grid->cells[y][x] && (live_neighbors < 2 || live_neighbors > 3))
                new_grid[y][x] = 0;
            else if (!grid->cells[y][x] && live_neighbors == 3)
                new_grid[y][x] = 1;
            else
                new_grid[y][x] = grid->cells[y][x];
        }
    }

    // Copy new grid to old grid and free the new grid
    for (int y = 0; y < grid->height; y++) {
        memcpy(grid->cells[y], new_grid[y], grid->width * sizeof(int));
    }
    free_grid(new_grid_struct);
}

// Load initial cell states from a file.
// This function reads a file containing cell positions (marked with 'O' and '.') and loads them into a grid.
// Skips lines starting with '!' as comments.
// Parameters:
// - const char *filename: Path to the file to read.
// Returns: A pointer to a Grid structure populated based on the file, or NULL if the file cannot be opened.
Grid *load_cells_from_file(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (!file) {
        fprintf(stderr, "Error opening file.\n");
        return NULL;
    }

    char line[1024];
    int max_x = 0, max_y = 0;
    int min_x = INT_MAX, min_y = INT_MAX;

    while (fgets(line, sizeof(line), file)) {
        if (line[0] == '!')
            continue;
        int x = 0, y = 0;
        for (char *p = line; *p; p++) {
            if (*p == 'O') {
                if (x < min_x) min_x = x;
                if (x > max_x) max_x = x;
                if (y < min_y) min_y = y;
                if (y > max_y) max_y = y;
            }
            if (*p == '\n') {
                y++;
                x = 0;
            } else {
                x++;
            }
        }
    }
    fseek(file, 0, SEEK_SET);

    int width = max_x - min_x + 1 + X_PADDING; // macro set during build.
    int height = max_y - min_y + 1 + Y_PADDING;
    Grid *grid = create_grid(width, height);

    int start_x = START_X_OFFSET - min_x; // to center the pattern
    int start_y = START_Y_OFFSET - min_y;
    int row = 0;
    while (fgets(line, sizeof(line), file)) {
        if (line[0] == '!') continue;
        int col = 0;
        for (char *p = line; *p; p++, col++) {
            if (*p == 'O') {
                grid->cells[start_y + row][start_x + col] = 1;
            }
        }
        row++;
    }
    fclose(file);
    return grid;
}

// Entry point of the program.
// This function handles initialization, main loop, and cleanup of the game.
// Arguments are expected to specify the path to the initial configuration file.
// Parameters:
// - int argc: The count of command-line arguments.
// - char **argv: An array of command-line argument strings.
int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <path_to_cells_file>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    enable_raw_mode();
    Grid *grid = load_cells_from_file(argv[1]);

    int paused = 0;

    while (1) {
        display_grid(grid);
        if (!paused) {
            update_grid(grid);
        }

        struct timeval tv = {0, 0};
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(STDIN_FILENO, &readfds);
        tv.tv_usec = 200000;  // 200 ms

        if (select(STDIN_FILENO + 1, &readfds, NULL, NULL, &tv) > 0) {
            char ch;
            if (read(STDIN_FILENO, &ch, 1) == 1) {
                if (ch == 'q') break;
                if (ch == 'p') paused = !paused;
                if (ch == 's') {
                    free_grid(grid);
                    grid = load_cells_from_file(argv[1]);
                }
                if (ch == '\x1B') {  // Check for escape character
                    char seq[3];
                    if (read(STDIN_FILENO, &seq[0], 1) == 1 && seq[0] == '[') {
                        if (read(STDIN_FILENO, &seq[1], 1) == 1) {
                            if (seq[1] == 'A') {  // Up arrow
                                resize_grid(grid, grid->width + 1, grid->height + 1);
                            }
                            if (seq[1] == 'B') {  // Down arrow
                                if (grid->width > 10 && grid->height > 10) {
                                    resize_grid(grid, grid->width - 1, grid->height - 1);
                                }
                            }
                        }
                    }
                }
            }
        }
        usleep(200000);
    }

    free_grid(grid);
    disable_raw_mode();
    return 0;
}